from hashlib import sha256
import base64
import Crypto.Random.random as rand

#@param : four strings
#@return the hash of those strings, as a string, in a base64 format
def gen_pwd(username, mainpwd, website, salt):
    try :
        str_to_hash = username+mainpwd+website+salt
    except:
        print("Could not process username+mainpwd+website+salt")
        return "0"
    hash_bytes = sha256(str_to_hash.encode('utf-8')).digest()
    return base64.b64encode(hash_bytes).decode('utf-8')

#Return str : a python long integer with 'size' random bits in base 16
def gen_salt(size=256):
    salt = rand.getrandbits(size)
    return hex(salt)