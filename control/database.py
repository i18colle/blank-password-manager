import sqlite3

PATH_TO_DATABASE='database.sqlite3'

def init_database():
    global dbconnection, dbcursor
    dbconnection = sqlite3.connect(PATH_TO_DATABASE)
    dbcursor = dbconnection.cursor()
    create_tables()

def create_tables():
    dbcursor.execute('''CREATE TABLE IF NOT EXISTS users(
                name text NOT NULL,
                PRIMARY KEY (name))''')
    dbconnection.commit()
    dbcursor.execute('''CREATE TABLE IF NOT EXISTS websites(
                    user text,
                    name text,
                    salt text,
                    PRIMARY KEY (user, name)
                ); ''')
    dbconnection.commit()

def add_user(username):
    dbcursor.execute('INSERT INTO users VALUES (?)', (username,))
    dbconnection.commit()
# Return the list of users
def get_users():
    dbcursor.execute('SELECT name FROM users')
    return [row[0] for row in dbcursor.fetchall()]
def delete_user(username):
    dbcursor.execute('DELETE FROM users WHERE name=?;', (username,))

def add_website(username, websitename, salt):
    dbcursor.execute('INSERT INTO websites VALUES (?,?,?)', (username, websitename, salt))
    dbconnection.commit()
# Return a dictionary {website : salt}
def get_websites(username):
    dict_website_salt=dict()
    dbcursor.execute('SELECT name, salt FROM websites WHERE user=?;', (username,))
    for row in dbcursor.fetchall():
        dict_website_salt[row[0]]=row[1]
    return dict_website_salt
def delete_website(username, website_name):
    dbcursor.execute('DELETE FROM websites WHERE user=? AND name=?;', (username, website_name))

def export_database(path):
    try:
        new_dbconnection = sqlite3.connect(path)
        new_dbcursor = new_dbconnection.cursor()
        # Create tables
        new_dbcursor.execute('''CREATE TABLE IF NOT EXISTS users(
                    name text NOT NULL,
                    PRIMARY KEY (name))''')
        new_dbconnection.commit()
        new_dbcursor.execute('''CREATE TABLE IF NOT EXISTS websites(
                        user text,
                        name text,
                        salt text,
                        PRIMARY KEY (user, name)
                    ); ''')
        new_dbconnection.commit()
        # Fill them
        dbcursor.execute('SELECT * FROM users;')
        for row in dbcursor.fetchall():
            new_dbconnection.execute('INSERT INTO users VALUES (?)', row)
            new_dbconnection.commit()
        dbcursor.execute('SELECT * FROM websites;')
        for row in dbcursor.fetchall():
            new_dbconnection.execute('INSERT INTO websites VALUES (?,?,?)', row)
            new_dbconnection.commit()
        print("Database Exported")
    except Exception as error:
        print("ERROR IN TRYING TO EXPORT DATABASE TO", path, ":", error)

def import_database(path):
    former_list_of_users = get_users()
    former_website_database=dict() # {user : {website : salt}}
    for user in former_list_of_users:
        former_website_database[user]=get_websites(user)
    try:
        new_dbconnection = sqlite3.connect(path)
        new_dbcursor = new_dbconnection.cursor()
        new_dbcursor.execute('SELECT * FROM users;')
        for row in new_dbcursor.fetchall():
            user=row[0]
            if user not in former_list_of_users:
                add_user(user)
        new_dbcursor.execute('SELECT * FROM websites;')
        for row in new_dbcursor.fetchall():
            if row[0] not in former_website_database.keys or row[1] not in former_website_database[row[0]]:
                # If the user has no website or if the website is not in the user's list of websites
                add_website(row[0],row[1],row[2])
    except Exception as error:
        print("ERROR IN TRYING TO IMPORT DATABASE FROM", path, ":", error)

def close_connection_to_db():
    dbconnection.close()