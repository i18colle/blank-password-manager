from PySide2.QtCore import QStringListModel

class ListOfServers(QStringListModel):
    def __init__(self):
        QStringListModel.__init__(self)
    
    def update(self):
        listofServers=["LOCAL ONLY","127.0.0.1"]#TODO Update from the database
        self.setStringList(listofServers)

    '''
    Remove the Server from the database and update itself
    @param Server : string
    '''
    def removeServer(self, Server):
        #TODO Remove Server from the database
        print("Server removed : ", Server)
        self.update()
    
    '''
    Add the Server in the database and update itself
    @param Server : string
    '''
    def addServer(self, Server):
        #TODO Add Server in the database
        print("Server added : ", Server)
        self.update()

