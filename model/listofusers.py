from PySide2.QtCore import QStringListModel
from control.database import get_users as get_users_from_db, add_user, delete_user

class ListOfUsers(QStringListModel):
    def __init__(self):
        QStringListModel.__init__(self)
    
    def update(self):
        self.setStringList(get_users_from_db())
    '''
    Remove the user from the database and update itself
    @param user : string
    '''
    def removeUser(self, user):
        delete_user(user)
        print("User removed : ", user)
        self.update()
    
    '''
    Add the user in the database and update itself
    @param user : string
    '''
    def addUser(self, user):
        add_user(user)
        print("User added : ", user)
        self.update()

