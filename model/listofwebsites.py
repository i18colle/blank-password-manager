from PySide2.QtCore import QStringListModel
from control.database import get_websites as get_websites_from_db, add_website as add_website_in_database, delete_website as delete_website_from_db
from control.globalvariables import GlobaleVariables
from control.pwdgenerator import gen_salt

class ListOfWebsites(QStringListModel):
    def __init__(self):
        QStringListModel.__init__(self)
    
    def update(self):
        listofWebsites=get_websites_from_db(GlobaleVariables.selected_user)
        self.setStringList(listofWebsites)

    '''
    Remove the Website from the database and update itself
    @param Website : string
    '''
    def removeWebsite(self, website_name):
        delete_website_from_db(GlobaleVariables.selected_user, website_name)
        print("Website removed : ", website_name)
        self.update()
    
    '''
    Add the Website in the database and update itself
    @param Website : string
    '''
    def addWebsite(self, website_name):
        add_website_in_database(GlobaleVariables.selected_user, website_name, gen_salt())
        print("Website added : ", website_name)
        self.update()

