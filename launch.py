import sys
from PySide2.QtWidgets import QApplication

from view.mainwindow import MainWindow
from control.database import init_database, close_connection_to_db


if __name__ == "__main__":

    init_database()
    app = QApplication(sys.argv)

    mainwindow = MainWindow()
    mainwindow.show()

    sys.exit(app.exec_())
    close_connection_to_db()