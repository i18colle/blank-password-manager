from PySide2.QtWidgets import (QGridLayout, QGroupBox, QListView, QLabel, QWidget, QDialogButtonBox,
                               QToolButton, QFormLayout, QLineEdit)
from PySide2.QtCore import Slot, Qt

from model.listofservers import ListOfServers as ListOfServersModel
from view.askconfirmation import ask_confirmation

class ManageServers(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        # Widgets
        self.listServersGroupBox=ListServersGroupBox()
        self.addServerGroupBox=AddServerGroupBox()
        self.okButtonBox=QDialogButtonBox(QDialogButtonBox.Ok)
        # Layout
        self.layout=QGridLayout()
        self.setLayout(self.layout)
        self.layout.addWidget(self.listServersGroupBox)
        self.layout.addWidget(self.addServerGroupBox)
        self.layout.addWidget(self.okButtonBox)
        # Connections
        self.okButtonBox.clicked.connect(self.gotoHomeScreen)
        self.listServersGroupBox.deleteButton.clicked.connect(self.removeSelectedServers)
        self.addServerGroupBox.button.clicked.connect(self.addServer)


    def update(self):
        self.listServersGroupBox.listServersModel.update()
    
    @Slot()
    def gotoHomeScreen(self):
        self.mainWindow().gotoHomeScreen()
    
    # @param : str
    @Slot()
    def addServer(self):
        new_Server=self.addServerGroupBox.lineEdit.text()
        self.listServersGroupBox.listServersModel.addServer(new_Server)
    
    @Slot()
    def removeSelectedServers(self):
        listView=self.listServersGroupBox.listServersListView
        for index in listView.selectedIndexes():
            index=index.row()
            Server = listView.model().stringList()[index]
            confirmation_message="Are you sure you want to delete \""+Server+ "\" from the list of Servers?"
            if ask_confirmation(confirmation_message):
                self.listServersGroupBox.listServersModel.removeServer(Server)
    
    def mainWindow(self):
        return self.parent().parent()

class ListServersGroupBox(QGroupBox):
    def __init__(self):
        QGroupBox.__init__(self, "List of Servers : ")
        # Widgets
        self.listServersListView=QListView()
        self.deleteButton=QToolButton()
        self.deleteButton.setText("Delete")
        self.deleteButton.adjustSize()
        # Layout
        self.layout=QGridLayout()
        self.setLayout(self.layout)
        self.layout.setAlignment(Qt.AlignRight)
        self.layout.addWidget(self.listServersListView)
        self.layout.addWidget(self.deleteButton)
        # Model
        self.listServersModel=ListOfServersModel()
        self.listServersListView.setModel(self.listServersModel)


class AddServerGroupBox(QGroupBox):
    def __init__(self):
        QGroupBox.__init__(self, "Add a Server : ")
        # Widgets
        self.lineEdit=QLineEdit()
        self.lineEdit.setPlaceholderText("Write the Servername here")
        self.button=QToolButton()
        self.button.setText("Add Server")
        self.button.adjustSize()
        # Layout
        self.mainLayout=QFormLayout()
        self.secondaryLayout=QGridLayout()
        self.secondaryLayout.setAlignment(Qt.AlignCenter)
        self.mainLayout.addRow(QLabel("New Server : "), self.lineEdit)
        self.secondaryLayout.addWidget(self.button)
        self.mainLayout.addRow(self.secondaryLayout)
        self.setLayout(self.mainLayout)

