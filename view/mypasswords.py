from PySide2.QtWidgets import (QGridLayout, QGroupBox, QListView, QLabel, QWidget, QDialogButtonBox,
                               QToolButton, QFormLayout, QLineEdit)
from PySide2.QtCore import Slot, Qt

from model.listofwebsites import ListOfWebsites as ListOfWebsitesModel
from view.askconfirmation import ask_confirmation
from control.globalvariables import GlobaleVariables

class MyPasswords(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        # Widgets
        self.listWebsiteGroupBox=ListWebsiteGroupBox()
        self.addWebsiteGroupBox=AddWebsiteGroupBox()
        self.cancelButtonBox=QDialogButtonBox(QDialogButtonBox.Cancel)
        # Layout
        self.layout=QGridLayout()
        self.setLayout(self.layout)
        self.layout.addWidget(self.listWebsiteGroupBox)
        self.layout.addWidget(self.addWebsiteGroupBox)
        self.layout.addWidget(self.cancelButtonBox)
        # Connections
        self.cancelButtonBox.clicked.connect(self.gotoHomeScreen)
        self.listWebsiteGroupBox.deleteButton.clicked.connect(self.removeSelectedWebsite)
        self.addWebsiteGroupBox.button.clicked.connect(self.addWebsite)
        self.listWebsiteGroupBox.getPwdButton.clicked.connect(self.gotoGetPassword)
        self.listWebsiteGroupBox.listWebsiteListView.clicked.connect(self.updateSelectedWebsite)

    def update(self):
        self.listWebsiteGroupBox.listWebsiteModel.update()
        self.listWebsiteGroupBox.setTitle("List of "+GlobaleVariables.selected_user+"'s websites: ")
        self.listWebsiteGroupBox.getPwdButton.setEnabled(False)
    
    @Slot()
    def gotoHomeScreen(self):
        self.mainWindow().gotoHomeScreen()
    @Slot()
    def gotoGetPassword(self):
        self.mainWindow().gotoGetPassword()
    @Slot()
    def updateSelectedWebsite(self):
        website_seleted=self.listWebsiteGroupBox.listWebsiteListView.currentIndex().data()
        GlobaleVariables.selected_website=website_seleted
        print("Website selected :", GlobaleVariables.selected_website)
        self.listWebsiteGroupBox.getPwdButton.setEnabled(True)
    # @param : str
    @Slot()
    def addWebsite(self):
        new_Website=self.addWebsiteGroupBox.lineEdit.text()
        self.listWebsiteGroupBox.listWebsiteModel.addWebsite(new_Website)
    
    @Slot()
    def removeSelectedWebsite(self):
        listView=self.listWebsiteGroupBox.listWebsiteListView
        for index in listView.selectedIndexes():
            index=index.row()
            website = listView.model().stringList()[index]
            confirmation_message="Are you sure you want to delete \""+website+ "\" from the list of Website?"
            if ask_confirmation(confirmation_message):
                self.listWebsiteGroupBox.listWebsiteModel.removeWebsite(website)
    
    def mainWindow(self):
        return self.parent().parent()

class ListWebsiteGroupBox(QGroupBox):
    def __init__(self):
        QGroupBox.__init__(self)
        # Widgets
        self.listWebsiteListView=QListView()
        self.deleteButton=QToolButton()
        self.deleteButton.setText("Delete")
        self.deleteButton.adjustSize()
        self.getPwdButton=QToolButton()
        self.getPwdButton.setText("Get Passowrd")
        self.getPwdButton.adjustSize()
        self.getPwdButton.setStyleSheet("font:bold")
        # Layout
        self.layout=QGridLayout()
        self.setLayout(self.layout)
        self.layout.setAlignment(Qt.AlignRight)
        self.layout.addWidget(self.listWebsiteListView)
        self.layout.addWidget(self.getPwdButton)
        self.layout.addWidget(self.deleteButton)
        # Model
        self.listWebsiteModel=ListOfWebsitesModel()
        self.listWebsiteListView.setModel(self.listWebsiteModel)


class AddWebsiteGroupBox(QGroupBox):
    def __init__(self):
        QGroupBox.__init__(self, "Add a website : ")
        # Widgets
        self.lineEdit=QLineEdit()
        self.lineEdit.setPlaceholderText("Write the website's name or its url here")
        self.button=QToolButton()
        self.button.setText("Add website")
        self.button.adjustSize()
        # Layout
        self.mainLayout=QFormLayout()
        self.secondaryLayout=QGridLayout()
        self.secondaryLayout.setAlignment(Qt.AlignCenter)
        self.mainLayout.addRow(QLabel("New website : "), self.lineEdit)
        self.secondaryLayout.addWidget(self.button)
        self.mainLayout.addRow(self.secondaryLayout)
        self.setLayout(self.mainLayout)

