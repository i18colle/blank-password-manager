from PySide2.QtWidgets import (QWidget, QLineEdit, QDialogButtonBox, QFormLayout, QLabel)
from PySide2.QtCore import Slot

from control.globalvariables import GlobaleVariables
from control.database import get_websites
from control.pwdgenerator import gen_pwd

class GetPassword(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.isDisplayingPwd=False
        #Widgets
        self.titleLabel=QLabel()
        self.titleLabel.setStyleSheet("font:bold")
        self.mainPwdLineEdit=QLineEdit()
        self.mainPwdLineEdit.setEchoMode(QLineEdit.Password)
        self.displayPwdTitleLabel=QLabel()
        self.displayPwdTitleLabel.setStyleSheet("font:bold")
        self.displayPwdTitleLabel.setText("Your password is : ")

        self.displayPwdLineEdit=QLineEdit()
        self.displayPwdLineEdit.setReadOnly(True)
        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok
                             | QDialogButtonBox.Cancel)
        #Layouts
        self.layout=QFormLayout()
        self.setLayout(self.layout)
        self.layout.addRow(self.titleLabel)
        self.layout.addRow(QLabel("Your main password : " ),
                self.mainPwdLineEdit)
        self.layout.addRow(self.displayPwdTitleLabel)
        self.layout.addRow(self.displayPwdLineEdit)
        self.layout.addRow(self.buttonBox)
        #Connections
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.goToPreviousScreen)

    def update(self):
        self.titleLabel.setText(
            "Generate %s's password for %s"\
                    %(GlobaleVariables.selected_user,
                    GlobaleVariables.selected_website)
        )
        self.isDisplayingPwd=False
        self.displayPwdLineEdit.hide()
        self.displayPwdTitleLabel.hide()
    
    def displayPwd(self, pwd):
        self.isDisplayingPwd=True
        self.displayPwdLineEdit.setText(pwd)
        self.displayPwdTitleLabel.show()
        self.displayPwdLineEdit.show()

    def forgetPwds(self):
        self.mainPwdLineEdit.setText("")
        self.displayPwdLineEdit.setText("")

    @Slot()
    def accept(self):
        if self.isDisplayingPwd:
            self.goToPreviousScreen()
        else :
            mainpwd=self.mainPwdLineEdit.text()
            salt = get_websites(GlobaleVariables.selected_user)\
                    [GlobaleVariables.selected_website]
            pwd = gen_pwd(
                GlobaleVariables.selected_user,
                mainpwd,
                GlobaleVariables.selected_website,
                salt
            )
            self.displayPwd(pwd)
    @Slot()
    def goToPreviousScreen(self):
        self.forgetPwds()
        self.update()
        self.mainWindow().gotoMyPasswords()

    def mainWindow(self):
        return self.parent().parent()