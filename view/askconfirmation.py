from PySide2.QtWidgets import QMessageBox

"""
Provide a modal dialog for asking the user a question and receiving an answer.
@param question : str
"""
def ask_confirmation(question, no_by_default=True):
    msgbox = QMessageBox(QMessageBox.Question, "Asking Confirmation", question)
    msgbox.addButton(QMessageBox.Yes)
    msgbox.addButton(QMessageBox.No)
    if no_by_default :
        msgbox.setDefaultButton(QMessageBox.No)
    reply = msgbox.exec()
    return reply==QMessageBox.Yes
