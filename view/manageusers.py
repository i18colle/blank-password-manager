from PySide2.QtWidgets import (QGridLayout, QGroupBox, QListView, QLabel, QWidget, QDialogButtonBox,
                               QToolButton, QFormLayout, QLineEdit)
from PySide2.QtCore import Slot, Qt

from model.listofusers import ListOfUsers as ListOfUsersModel
from view.askconfirmation import ask_confirmation

class ManageUsers(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        # Widgets
        self.listUsersGroupBox=ListUsersGroupBox()
        self.addUserGroupBox=AddUserGroupBox()
        self.okButtonBox=QDialogButtonBox(QDialogButtonBox.Ok)
        # Layout
        self.layout=QGridLayout()
        self.setLayout(self.layout)
        self.layout.addWidget(self.listUsersGroupBox)
        self.layout.addWidget(self.addUserGroupBox)
        self.layout.addWidget(self.okButtonBox)
        # Connections
        self.okButtonBox.clicked.connect(self.gotoHomeScreen)
        self.listUsersGroupBox.deleteButton.clicked.connect(self.removeSelectedUsers)
        self.addUserGroupBox.button.clicked.connect(self.addUser)


    def update(self):
        self.listUsersGroupBox.listUsersModel.update()
    
    @Slot()
    def gotoHomeScreen(self):
        self.mainWindow().gotoHomeScreen()
    
    # @param : str
    @Slot()
    def addUser(self):
        new_user=self.addUserGroupBox.lineEdit.text()
        self.listUsersGroupBox.listUsersModel.addUser(new_user)
    
    @Slot()
    def removeSelectedUsers(self):
        listView=self.listUsersGroupBox.listUsersListView
        for index in listView.selectedIndexes():
            index=index.row()
            user = listView.model().stringList()[index]
            confirmation_message="Are you sure you want to delete \""+user+ "\" from the list of users?"
            if ask_confirmation(confirmation_message):
                self.listUsersGroupBox.listUsersModel.removeUser(user)
    
    def mainWindow(self):
        return self.parent().parent()

class ListUsersGroupBox(QGroupBox):
    def __init__(self):
        QGroupBox.__init__(self, "List of users : ")
        # Widgets
        self.listUsersListView=QListView()
        self.deleteButton=QToolButton()
        self.deleteButton.setText("Delete")
        self.deleteButton.adjustSize()
        # Layout
        self.layout=QGridLayout()
        self.setLayout(self.layout)
        self.layout.setAlignment(Qt.AlignRight)
        self.layout.addWidget(self.listUsersListView)
        self.layout.addWidget(self.deleteButton)
        # Model
        self.listUsersModel=ListOfUsersModel()
        self.listUsersListView.setModel(self.listUsersModel)


class AddUserGroupBox(QGroupBox):
    def __init__(self):
        QGroupBox.__init__(self, "Add a user : ")
        # Widgets
        self.lineEdit=QLineEdit()
        self.lineEdit.setPlaceholderText("Write the username here")
        self.button=QToolButton()
        self.button.setText("Add user")
        self.button.adjustSize()
        # Layout
        self.mainLayout=QFormLayout()
        self.secondaryLayout=QGridLayout()
        self.secondaryLayout.setAlignment(Qt.AlignCenter)
        self.mainLayout.addRow(QLabel("New user : "), self.lineEdit)
        self.secondaryLayout.addWidget(self.button)
        self.mainLayout.addRow(self.secondaryLayout)
        self.setLayout(self.mainLayout)

