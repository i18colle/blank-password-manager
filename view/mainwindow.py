from PySide2.QtWidgets import QWidget, QStackedWidget, QVBoxLayout

from view.homescreen import HomeScreen
from view.manageusers import ManageUsers
from view.mypasswords import MyPasswords
from view.manageservers import ManageServers
from view.getpassword import GetPassword

WINDOW_TITLE="Blank Password Manager"

class MainWindow(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.setWindowTitle(WINDOW_TITLE)

        self.stackedWidget=QStackedWidget()
        self.homeScreen = HomeScreen()
        self.stackedWidget.addWidget(self.homeScreen)
        self.manageUsers = ManageUsers()
        self.stackedWidget.addWidget(self.manageUsers)
        self.mypasswords=MyPasswords()
        self.stackedWidget.addWidget(self.mypasswords)
        self.manageServers = ManageServers()
        self.stackedWidget.addWidget(self.manageServers)
        self.getPassword = GetPassword()
        self.stackedWidget.addWidget(self.getPassword)

        self.layout=QVBoxLayout()
        self.setLayout(self.layout)
        self.layout.addWidget(self.stackedWidget)

        self.gotoHomeScreen()
    
    def gotoHomeScreen(self):
        self.homeScreen.update()
        self.stackedWidget.setCurrentWidget(self.homeScreen)
    
    def gotoManageUsers(self):
        self.manageUsers.update()
        self.stackedWidget.setCurrentWidget(self.manageUsers)
    
    def gotoMyPasswords(self):
        self.mypasswords.update()
        self.stackedWidget.setCurrentWidget(self.mypasswords)

    def gotoManageServers(self):
        self.manageServers.update()
        self.stackedWidget.setCurrentWidget(self.manageServers)

    def gotoGetPassword(self):
        self.getPassword.update()
        self.stackedWidget.setCurrentWidget(self.getPassword)
