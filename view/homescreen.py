from PySide2.QtWidgets import (QLabel, QPushButton, QComboBox, QGroupBox, QFormLayout,
                               QGridLayout, QWidget, QDialogButtonBox, QSizePolicy,
                               QToolButton, QFileDialog)
from PySide2.QtCore import Slot, Qt
from PySide2.QtGui import QPixmap

from view.manageusers import ManageUsers
from control.globalvariables import GlobaleVariables
from control.database import export_database, import_database
from model.listofusers import ListOfUsers as ListOfUsersModel

PATH_TO_IMAGE="images/logo_400x300.png"

class HomeScreen(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        # Widgets
        self.leftImageLabel=LeftImageLabel()
        # Layout
        self.layout=QGridLayout()
        self.setLayout(self.layout)
        self.rightLayout=RightLayout()
        self.layout.addLayout(self.rightLayout, 0, 1)
        self.layout.addWidget(self.leftImageLabel, 0, 0)
        # Connections
        self.rightLayout.managePasswordsAndUsersGroupBox.manageUsersButton\
                .clicked.connect(self.gotoManageUsers)
        self.rightLayout.managePasswordsAndUsersGroupBox.myPasswordsButton.clicked.connect(self.gotoMyPasswords)
        self.rightLayout.server_Import_Export_GroupBox.importDatabaseButton.clicked.connect(self.importDatabase)
        self.rightLayout.server_Import_Export_GroupBox.exportDatabaseButton.clicked.connect(self.exportDatabase)
        self.rightLayout.server_Import_Export_GroupBox.manageServersButton.clicked.connect(self.gotoManageServers)
        self.rightLayout.managePasswordsAndUsersGroupBox.selectUserComboBox\
                .currentTextChanged.connect(self.update_selected_user)
        self.rightLayout.quitButtonBox.clicked.connect(self.quit)

    def update(self):
        self.rightLayout.managePasswordsAndUsersGroupBox.selectUserComboBox.update()

    @Slot()
    def update_selected_user(self):
        if self.rightLayout.managePasswordsAndUsersGroupBox.selectUserComboBox\
                .currentIndex()==0 : # "<Select user>" is selected
            self.rightLayout.managePasswordsAndUsersGroupBox.myPasswordsButton.setEnabled(False)
        else :    
            GlobaleVariables.selected_user=\
                    self.rightLayout.managePasswordsAndUsersGroupBox.selectUserComboBox.currentText()
            print("User selected : ", GlobaleVariables.selected_user)
            self.rightLayout.managePasswordsAndUsersGroupBox.myPasswordsButton.setEnabled(True)
            self.rightLayout.managePasswordsAndUsersGroupBox.myPasswordsButton.setDefault(True)

    @Slot()
    def gotoManageUsers(self):
        self.mainWindow().gotoManageUsers()
    @Slot()
    def gotoMyPasswords(self):
        self.mainWindow().gotoMyPasswords()
    @Slot()
    def gotoManageServers(self):
        self.mainWindow().gotoManageServers()
    @Slot()
    def importDatabase(self):
        path = QFileDialog.getOpenFileName(self, "Select the database file") [0]
        print("Trying to import", path,"...")
        import_database(path)
        self.update()
    @Slot()
    def exportDatabase(self):
        path = QFileDialog.getSaveFileName(self, "Where do you want to save your database file?") [0]
        print("Trying to export the database to", path,"...")
        export_database(path)
    @Slot()
    def quit(self):
        self.mainWindow().close()

    def mainWindow(self):
        return self.parent().parent()

class LeftImageLabel(QLabel):
    def __init__(self):
        QLabel.__init__(self)

        self.setPixmap(
                QPixmap(PATH_TO_IMAGE)
        )


class RightLayout(QGridLayout):
    def __init__(self):
        QGridLayout.__init__(self)
        # Widgets
        self.managePasswordsAndUsersGroupBox = ManagePasswordsAndUsersGroupBox()
        self.server_Import_Export_GroupBox = Server_Import_Export_GroupBox()
        self.quitButtonBox=QDialogButtonBox(QDialogButtonBox.Close)
        # Layout
        self.addWidget(self.managePasswordsAndUsersGroupBox)
        self.addWidget(self.server_Import_Export_GroupBox)
        self.addWidget(self.quitButtonBox)


class ManagePasswordsAndUsersGroupBox(QGroupBox):
    def __init__(self):
        QGroupBox.__init__(self, "Access to my passwords")
        # Widgets
        self.selectUserComboBox = SelectUserComboBox()
        self.manageUsersButton = QToolButton()
        self.manageUsersButton.setText(" Add/Delete Users ")
        self.manageUsersButton.adjustSize
        self.myPasswordsButton = QPushButton("My Passwords")
        self.myPasswordsButton.setEnabled(False)
        self.myPasswordsButton.setStyleSheet("font:bold")
        # Layout
        self.mainLayout=QFormLayout()
        self.secondaryLayout=QGridLayout()
        self.secondaryLayout.setAlignment(Qt.AlignCenter)
        self.mainLayout.addRow(QLabel("Select user : "), self.selectUserComboBox)
        self.secondaryLayout.addWidget(self.manageUsersButton, 0, 0)
        self.secondaryLayout.addWidget(self.myPasswordsButton, 1, 0)
        self.mainLayout.addRow(self.secondaryLayout)
        self.setLayout(self.mainLayout)

class Server_Import_Export_GroupBox(QGroupBox):
    def __init__(self):
        QGroupBox.__init__(self, "Parameters")
        # Widgets
        self.manageServersButton = QPushButton(" Add / Delete Servers ")
        self.importDatabaseButton = QPushButton("Import Database")
        self.exportDatabaseButton = QPushButton("Export Database")
        # Layout
        self.layout=QFormLayout()
        self.layout.addRow(self.importDatabaseButton, self.exportDatabaseButton)
        self.layout.addRow(self.manageServersButton)
        self.setLayout(self.layout)


class SelectUserComboBox(QComboBox):
    def __init__(self):
        QComboBox.__init__(self)
        self.listOfUsersModel=ListOfUsersModel()
        #self.update()
    def update(self):
        self.clear()
        self.insertItem(0, "<Select user>")
        self.listOfUsersModel.update()
        self.insertItems(1, self.listOfUsersModel.stringList())





